import { resolve } from "path";
import {fileExtensions, FileInfoMap, FileTypes, getTestFileNames} from "./util";
import { readdirSync } from 'fs'
import { promises } from "fs";

export class FilesParser {
	private readonly fileInfo: FileInfoMap;

	constructor() {
		this.fileInfo = {};
		this.fileInfo = getTestFileNames(readdirSync(resolve('server', 'tasks')));
	}

	public getTasks(): Array<string> {
		return Object.keys(this.fileInfo);
	}

	public getModuleContent(taskName: string): Object {
		return require(`./tasks/${taskName}.spec.js`);
	}

	public async getModuleDescription(taskName: string): Promise<string> {
		let fileName = `./server/tasks/${taskName}.md`;
		return (await promises.readFile(fileName)).toString();
	}
}
