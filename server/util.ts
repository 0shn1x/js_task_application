export enum FileTypes {
	TEST = 'test',
	MARKDOWN = 'markdown'
}
//TODO generate regExp from fileExtensionsObject
export let fileExtensions = {
	[FileTypes.TEST]: '.spec.ts',
	[FileTypes.MARKDOWN]: '.md'
};

const parseFilesConfig: Array<ParserConfig> = [
	{
		regExp: /^([A-Za-z]*)\.spec\.js$/,
		name: FileTypes.TEST
	},
	{
		regExp: /^([A-Za-z]*)\.md$/,
		name: FileTypes.MARKDOWN
	}
];

interface ParserConfig {
	regExp: RegExp;
	name: FileTypes;
}

export type FileInfoMap = {[key: string]: number}

export function getTestFileNames(fileNames: Array<string>): FileInfoMap {
	let result: FileInfoMap = {};
	fileNames.forEach(fileName => {
		for (let parser of parseFilesConfig) {
			let matched = fileName.match(parser.regExp);
			if (matched !== null) {
				let [, key] = matched;
				result[key] = result.hasOwnProperty(key) ? result[key] + 1 : 1;
			}
		}
	});
	for (let key in result) {
		if (result[key] !== parseFilesConfig.length) {
			delete result[key];
		}
	}
	return result
}
