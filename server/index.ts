import * as WebSocket from "ws";
import {createServer} from "http";
import {FilesParser} from "./filesParser";

const server = createServer();
const ws: WebSocket.Server = new WebSocket.Server({server});

const fileParser = new FilesParser();
let tasks = fileParser.getTasks();
fileParser.getModuleContent(tasks[0]);

ws.on('connection', (socket: WebSocket) => {
	fileParser.getModuleDescription('prototype')
		.then((result: string) => {
			socket.send(JSON.stringify({
				type: 'content',
				content: result
			}))
		});

	socket.on('message', (msg: string) => {
		let data;
		try {
			data = JSON.parse(msg);
			console.log(data);
		} catch (error) {
			console.error(error);
			return;
		}
	});
});

server.listen(process.env.PORT);
