import * as vm from 'vm';

export class VmRunner {
	run(func: object) {
		let context = {};
		vm.createContext(context);
		try {
			vm.runInContext(func.toString(), context);
		} catch (error) {
			console.error(error);
		}
	}
}
