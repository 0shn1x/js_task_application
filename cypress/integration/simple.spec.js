describe('My First Test', () => {
	it('Click an element', () => {
		cy.visit('http://207.154.254.134/js/#/');

		cy.contains('Tester').click();

		cy.url().should('include', 'tester');

		cy.get('textarea.js-editor-textarea').type('function');
	});
});
