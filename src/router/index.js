import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';
import Tester from '@/components/Tester';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'HelloWorld',
			component: HelloWorld
		},
		{
			path: '/tester',
			name: 'Tester',
			component: Tester
		}
	]
});
