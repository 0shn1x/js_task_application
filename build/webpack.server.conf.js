const path = require('path');
const webpack = require('webpack');

module.exports = {
	entry: path.resolve(__dirname, '../server/index.ts'),
	target: 'node',
	mode: 'development',
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: 'awesome-typescript-loader',
				exclude: /node_modules/
			}
		]
	},
	resolve: {
		extensions: [ '.ts', '.js' ]
	},
	output: {
		filename: 'server.js',
		path: path.resolve('./')
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env.PORT': 8080
		})
	]
};
